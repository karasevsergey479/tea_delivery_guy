package net.pbt.tea_delivery_guy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TeaDeliveryGuyApplication {

    public static void main(String[] args) {
        SpringApplication.run(TeaDeliveryGuyApplication.class, args);
    }

}
